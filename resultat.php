<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <title>Affichage des données</title>
</head>
<body>
    

<?php

// header("Refresh:0; url=page2.php");
// Ouverture de la connexion à la base de donnée en définissant les constantes
  $host = 'localhost';
  $dbname = 'formulaire';
  $username = 'root';
  $password = '';


//connexion à la base de données
//instanciation d'un objet PDO pour créer une connexion à la BDD
$bdd = new PDO('mysql:host='.$host.';dbname='.$dbname.';charset=utf8', $username, $password);
$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);




// la fonction prepare prend toujours du mySQL
$resultat = $bdd->prepare("SELECT * FROM t_msg INNER JOIN t_personnes ON t_personnes.id_Email = t_msg.id_Email INNER JOIN t_email ON t_msg.id_Email = t_email.id_Email ORDER BY t_msg.date_msg DESC");
//envoi et serveur mysql execute
$resultat->execute();

$arraysecure = [];

if (!$resultat){

    printf("Il y a un problème dans la récupération des données !") ;
}else{
  //php récupère les données avec fetch
   while($BigData=$resultat->fetch(PDO::FETCH_ASSOC)){

   filter_var($BigData["id_Email"], FILTER_SANITIZE_NUMBER_INT);   
   filter_var($BigData["nom"], FILTER_SANITIZE_STRING);
   filter_var($BigData["prenom"], FILTER_SANITIZE_STRING);
   filter_var($BigData["tel"], FILTER_SANITIZE_STRING);
   filter_var($BigData["Email"], FILTER_SANITIZE_EMAIL);
   filter_var($BigData["sujet"], FILTER_SANITIZE_STRING);
   filter_var($BigData["msg"], FILTER_SANITIZE_STRING); 
   filter_var($BigData["etat"], FILTER_SANITIZE_STRING);
   filter_var($BigData["id_msg"], FILTER_SANITIZE_NUMBER_INT);
   array_push($arraysecure, $BigData);
   }

   ?>
    <table>

<div class="container">
  <h2>Données Formulaire de Contact</h2>
  <p></p>            
  <table class="table table-hover">
    <thead>
      <tr>
        <th>id Email</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>N° tel</th>
        <th>Email</th>
        <th>Sujet</th>
        <th>Message</th>
        <th>Etat</th>
        <th>Traitement données</th>
        <th>Modifier état</th>
      </tr>
    </thead>
    <tbody>
      
   



<?php


foreach($arraysecure as $element){
    ?>
 
<tr> 
    <td><?php printf($element["id_Email"]);?></td>    
    <td><?php printf($element["nom"]);?></td> 
    <td><?php printf($element["prenom"]);?></td> 
    <td><?php printf($element["tel"]);?></td>
    <td><?php printf($element["Email"]);?></td> 
    <td><?php printf($element["sujet"]);?></td> 
    <td><?php printf($element["msg"]);?></td> 
    <td><?php printf($element["etat"]);?></td>
    
    
    
<td>
  <!--les name value servent à envoyer les données dans le $POST-->
<form action="resultat.php" method="post">
  <!--clé = name, value doit être dans la même balise que le name-->
<select name="DataForm" id="pet-select">
  
    <option value="à traiter">A traiter</option>
    <option value="À relancer">À relancer</option>
    <option value="En attente de réponse">En attente de réponse</option>
    <option value="RDV pris">RDV pris</option>
    <option value="Sans suite">Sans suite</option>
    
    
</select>

</td>
<td>
<button type="submit" name="id_msg" value="<?php printf($element['id_msg']);?>" class="btn btn-outline-info ">Modifier état</button>
</td></form>
</tr>

    <?php
   
}


?> 

</tbody>
  </table>
</div>
    
    <?php
    // closeCursor — Ferme le curseur, permettant à la requête d'être de nouveau exécutée 
    $resultat->closeCursor();
    
}

?>
<?php
//récupération des données du formulaire
//isset=si le champs existe
    if (isset($_POST['DataForm']) && !empty($_POST['DataForm']))
    {

        
            $DataForm = filter_var($_POST['DataForm'], FILTER_SANITIZE_STRING);
      
            $id = filter_var($_POST['id_msg'], FILTER_SANITIZE_STRING);
//set = attribuer une valeur a quelque chose qui existe déjà
//on fait le liens entre le formulaire et la base de donnée pour cvhanger assigner l'état à l'ID msg
            $update = $bdd->prepare('UPDATE t_msg SET etat = :etat WHERE id_msg = :id_msg');

            try
            {                
//begin transaction et commit n e prennet jamais d'argument
                $bdd->beginTransaction();
                $update->execute([":etat" =>$DataForm, ":id_msg" =>$id ]);
                //maintenant que les commandes SQL sont exécutées on les envoi au serveur de base de données
                $bdd->commit();
                header("Refresh:0");            

            }catch (Exception $erreur) {
                printf('Erreur ' . $erreur->getMessage());
                $bdd->rollBack();
            }
            
    } 
?>

</body>
</html>

