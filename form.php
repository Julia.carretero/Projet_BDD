<?php

if (isset($_POST["email"]) && isset($_POST["message"]) && isset($_POST["sujet"])) {

    if (!empty($_POST["email"]) && !empty($_POST["message"]) && !empty($_POST["sujet"])) {

        $nom = $_POST["nom"];

        $prenom = $_POST["prenom"];

        $telephone = $_POST["telephone"];

        $email = $_POST["email"];

        $sujet = $_POST["sujet"];

        $petitMot = $_POST["message"];

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            if (filter_var($email, FILTER_SANITIZE_EMAIL)) {

                if (filter_var($petitMot, FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {

                    $petitMot = filter_var($petitMot, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
                    $sujet = filter_var($sujet, FILTER_SANITIZE_STRING);

                    if (isset($prenom)) {

                        if (!empty($prenom)) {

                            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
                        }
                    }

                    if (isset($nom)) {

                        if (!empty($nom)) {

                            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
                        }
                    }

                    if (isset($telephone)) {

                        if (!empty($telephone)) {

                            filter_var($telephone, FILTER_SANITIZE_STRIPPED, array("options" => array('min_range' => 10, 'max_range' => 10)));

                            $telephone = filter_var($telephone, FILTER_SANITIZE_STRIPPED, array("options" => array('min_range' => 10, 'max_range' => 10)));
                        }
                    }

                    // quand mon site sera en ligne je devrais mettre ça

/*<?php
$bdd = new PDO('mysql:host=sql.hebergeur.com;dbname=mabase;charset=utf8', 'pierre.durand', 's3cr3t');
?>*/



try
{

// Sous WAMP
// pour travailler en local utiliser DVeavber
	$bdd = new PDO('mysql:host=localhost;dbname=formulaire;charset=utf8', 'root', '');
}
catch (Exception $erreur)
{
        die('Erreur : ' . $erreur->getMessage());
}
//variable de préparation d'envoi= on prepare l'insertion de données dans la table le champ email le mail dont on va prendre la valeur ailleurs
$AddMail = $bdd->prepare("INSERT INTO t_email (Email) VALUES (:variable_email)");
// je dois déclarer les clés primaires et étrangère donc je déclare ma variable emailID qui est ma clé primaire en récupérant l'ID en lagage SQL
// set = déclarer une variable en sql, la fonction me retourne l'ID de l'email qu'on vient de rentrer
$emailID = $bdd->prepare("SET @emailID = (SELECT id_Email FROM t_email WHERE Email = :variable_email)");
$AddMsg = $bdd->prepare("INSERT INTO t_msg (sujet, msg, id_Email) VALUES (:variable_sujet, :variable_msg, @emailID)");
$AddPersonnes = $bdd->prepare("INSERT INTO t_personnes (prenom, nom, tel, id_Email) VALUES (:variable_prenom, :variable_nom, :variable_tel, @emailID) ");


// faire une transaction


try
{
    // tableau clé => valeur
    $mailreçu=[":variable_email" =>$email];
    $messagebdd=[":variable_sujet" =>$sujet, ":variable_msg" =>$petitMot];
    $personnes=[":variable_prenom" =>$prenom, ":variable_nom" =>$nom, ":variable_tel" =>$telephone];
    // a partir de maintenant s'il y a un msg d'erreur on y a accès
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   // permet de recevoir des messages avec deux email similaires 
//    je dois recupérer l'email qu'on vient de rentrer dans le formulaire 
$checkreq= $bdd->prepare("SELECT id_Email FROM t_email WHERE Email = :variable_email");
$checkreq->execute($mailreçu);

// foreach($checkreq->fetchall() as $recupmail){
//     //si l'email récupéré est = au dollar POST de email alors c'est vrai sinon rien
//     if ($recupmail==$email){
//         $checkmail=true;
//     }
// }
// //si le mail est déja dans la base de données
// if($checkmail=true){
//     //on envoi les données à la base de données sauf le mail

    $bdd->beginTransaction();
    $AddMail->execute($mailreçu);
    $emailID->execute($mailreçu);
    $AddMsg->execute($messagebdd);
    $AddPersonnes->execute($personnes);
    
    //maintenant que les commandes SQL sont exécutées on les envoi au serveur de base de données
    $bdd->commit();
// }else{

// //sinon on envoi tout avec le mail

// //on envoi les données à la base de données

//     $bdd->beginTransaction();
//     $AddMail->execute($mailreçu);
//     $emailID->execute($mailreçu);
//     $AddMsg->execute($messagebdd);
//     $AddPersonnes->execute($personnes);
    
//     //maintenant que les commandes SQL sont exécutées on les envoi au serveur de base de données
//     $bdd->commit();
// }
}
catch (Exception $erreur)
{
    printf($erreur->getMessage());
    $bdd->rollBack();
}

$to      = "carretero.julia@gmail.com";

$subject = "Message de contact de la part de $email";

$message = $petitMot;

$headers = "From: Contact@Julia.Carretero.com" . PHP_EOL . 'Content-type: text/html; charset=utf-8' . PHP_EOL .

    'X-Mailer: PHP/' . phpversion();

if (!mail($to, $subject, $message, $headers)) {

printf("Mail non envoyé");

}

                }
            }
        }
    }else  {
        printf("Merci de revenir en arrière et remplir les champs obligatoires");
    }
} else {
    printf("Ne pas modifier les champs du formulaire");
}


?>